import requests


def test_put_bets_state_ok(bet_maker_url: str) -> None:
    body = {
        "state": "2",
    }
    response = requests.put(f"{bet_maker_url}/bets/1/change_state", json=body)
    assert response.status_code == 200
