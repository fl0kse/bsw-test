import random
import string

import bson


def rnd_objectid():
    return bson.ObjectId()


def rnd_float():
    return round(float(f"{random.randint(0, 100)}.{random.randint(0, 99)}"), 2)


def rnd_string(length: int = 16):
    return "".join(
        random.choice(string.ascii_letters + string.digits) for _ in range(length)
    )


def rnd_event_state():
    return random.randint(1, 3)
