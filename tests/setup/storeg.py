from typing import Any

from tests.setup import rnd_event_state, rnd_float, rnd_objectid, rnd_string


def random_bet(**kwargs: Any) -> dict[str, Any]:
    bet = {
        "bet_id": rnd_objectid(),
        "event_id": rnd_string(),
        "user_id": rnd_string(),
        "amount": rnd_float(),
        "coefficient": rnd_float(),
        "state": rnd_event_state(),
    }
    bet.update(**kwargs)
    return bet
