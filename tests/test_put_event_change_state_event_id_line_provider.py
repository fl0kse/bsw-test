import requests


def test_change_state_ok(line_provider_url: str) -> None:
    body = {
        "state": 2,
    }
    response = requests.put(f"{line_provider_url}/event/change_state/1", json=body)
    assert response.status_code == 200
    body = {
        "state": 1,
    }
    response = requests.put(f"{line_provider_url}/event/change_state/1", json=body)
    assert response.status_code == 409
