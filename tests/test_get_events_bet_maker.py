import requests

from bet_maker.api.models import EventList


def test_get_events_ok(bet_maker_url: str) -> None:
    response = requests.get(f"{bet_maker_url}/events")
    assert response.status_code == 200
    json_resp = response.json()
    EventList(**json_resp)
