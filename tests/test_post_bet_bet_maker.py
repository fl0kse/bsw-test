import requests


def test_post_bet_ok(bet_maker_url: str) -> None:
    body = {"event_id": "1", "amount": 2.25}
    response = requests.post(f"{bet_maker_url}/bet", json=body)
    assert response.status_code == 201


def test_bet_large_coefficient(bet_maker_url: str) -> None:
    body = {"event_id": "1", "amount": 2.2525}
    response = requests.post(f"{bet_maker_url}/bet", json=body)
    assert response.status_code == 400


def test_bet_event_not_found(bet_maker_url: str) -> None:
    body = {"event_id": "1000001", "amount": 2.25}
    response = requests.post(f"{bet_maker_url}/bet", json=body)
    assert response.status_code == 404
