import requests
from pymongo import MongoClient

from bet_maker.api.models import BetList
from tests.setup.storeg import random_bet


def test_put_bets_state_ok(bet_maker_url: str, db: MongoClient) -> None:
    bets = [random_bet(user_id="12345") for i in range(5)]
    db.bets.insert_many(bets)
    response = requests.get(f"{bet_maker_url}/bets/12345")
    assert response.status_code == 200
    json_resp = response.json()
    response_model = BetList(**json_resp)
    inserted_model = BetList(bets=bets)
    assert response_model == inserted_model
