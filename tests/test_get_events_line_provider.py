import requests

from line_provider.api.models import EventList


def test_get_events_ok(line_provider_url: str) -> None:
    response = requests.get(f"{line_provider_url}/events")
    assert response.status_code == 200
    json_resp = response.json()
    EventList(**json_resp)
