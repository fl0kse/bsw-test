import requests


def test_post_event_ok(line_provider_url: str) -> None:
    body = {
        "event_id": "4",
        "coefficient": 2.25,
        "deadline": 60,
        "state": 1,
    }
    response = requests.post(f"{line_provider_url}/event", json=body)
    assert response.status_code == 201


def test_event_large_coefficient(line_provider_url: str) -> None:
    body = {
        "event_id": "5",
        "coefficient": 2.2525,
        "deadline": 60,
        "state": 1,
    }
    response = requests.post(f"{line_provider_url}/event", json=body)
    assert response.status_code == 400


def test_event_exist_event_id(line_provider_url: str) -> None:
    body = {
        "event_id": "1",
        "coefficient": 2.25,
        "deadline": 60,
        "state": 1,
    }
    response = requests.post(f"{line_provider_url}/event", json=body)
    assert response.status_code == 409
