import requests

from line_provider.api.models import Event


def test_get_event_ok(line_provider_url: str) -> None:
    response = requests.get(f"{line_provider_url}/event/1")
    assert response.status_code == 200
    json_resp = response.json()
    Event(**json_resp)


def test_get_event_bad(line_provider_url: str) -> None:
    response = requests.get(f"{line_provider_url}/event/1000001")
    assert response.status_code == 404
