import time
from decimal import Decimal

from aiohttp_requests import requests
from fastapi import APIRouter, Body, HTTPException, Path, Response

from . import models
from .config import Settings

events: dict[str, models.Event] = {
    "1": models.Event(
        event_id="1",
        coefficient=1.2,
        deadline=int(time.time()) + 600,
        state=models.EventState.NEW,
    ),
    "2": models.Event(
        event_id="2",
        coefficient=1.15,
        deadline=int(time.time()) + 60,
        state=models.EventState.NEW,
    ),
    "3": models.Event(
        event_id="3",
        coefficient=1.67,
        deadline=int(time.time()) + 90,
        state=models.EventState.NEW,
    ),
}

routers = APIRouter()


@routers.post("/event")
async def create_event(event: models.Event) -> Response:
    if event.event_id in events:
        raise HTTPException(status_code=409, detail="Event already exist")

    if Decimal(str(event.coefficient)).as_tuple().exponent * (-1) > 2:
        raise HTTPException(status_code=400, detail="Too many decimal places")

    events[event.event_id] = event
    return Response(status_code=201)


@routers.put("/event/change_state/{event_id}")
async def change_state(
    event_id: str = Path(), event_result_response: models.EventResultResponse = Body()
) -> Response:
    if event_id not in events:
        raise HTTPException(status_code=404, detail="Event not found")

    if getattr(events[event_id], "state") is not models.EventState.NEW:
        raise HTTPException(status_code=409, detail="Event already finished")

    setattr(events[event_id], "state", event_result_response.state)

    response = await requests.put(
        f"{Settings().BET_MAKER_URL}/bets/{event_id}/change_state",
        json={"state": event_result_response.state},
    )
    if not response.status == 200:
        raise HTTPException(status_code=400, detail="Can't update bets state")

    return Response(status_code=200)


@routers.get(
    "/event/{event_id}", response_model=models.Event, response_model_by_alias=False
)
async def get_event(event_id: str = Path()) -> models.Event:
    if event_id not in events:
        raise HTTPException(status_code=404, detail="Event not found")

    return events[event_id]


@routers.get("/events", response_model=models.EventList, response_model_by_alias=False)
async def get_events() -> models.EventList:
    return models.EventList(
        events=list(e for e in events.values() if time.time() < e.deadline)  # type: ignore
    )
