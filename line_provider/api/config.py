from pydantic import BaseSettings


class Settings(BaseSettings):
    BET_MAKER_URL: str
