import decimal
from enum import IntEnum

from pydantic import BaseModel


class EventState(IntEnum):
    NEW = 1
    FINISHED_WIN = 2
    FINISHED_LOSE = 3


class Event(BaseModel):
    event_id: str
    coefficient: decimal.Decimal
    deadline: int
    state: EventState = EventState.NEW


class EventList(BaseModel):
    events: list[Event]


class EventResultResponse(BaseModel):
    state: EventState
