import uvicorn
from api.endpoints import routers
from fastapi import FastAPI

logger_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        },
        "access": {
            "()": "uvicorn.logging.DefaultFormatter",
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
        "access": {
            "formatter": "access",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
        },
    },
    "loggers": {
        "uvicorn.error": {
            "level": "INFO",
            "handlers": [
                "default",
            ],
            "propagate": "no",
        },
        "uvicorn.access": {"level": "INFO", "handlers": ["access"], "propagate": "no"},
    },
}

if __name__ == "__main__":
    app = FastAPI(title="line-provider")

    app.include_router(routers)

    uvicorn.run(app, host="0.0.0.0", port=8081, log_config=logger_config)
