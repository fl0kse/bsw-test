from decimal import Decimal

from aiohttp_requests import requests
from fastapi import APIRouter, Body, HTTPException, Path, Response

from . import models, storage
from .config import Settings

routers = APIRouter()


@routers.get("/events", response_model=models.EventList, response_model_by_alias=False)
async def get_events() -> models.EventList:
    response = await requests.get(f"{Settings().LINE_PROVIDER_URL}/events")
    return await response.json()


@routers.post("/bet")
async def create_bet(bet_response: models.BetResponse = Body()) -> Response:
    if Decimal(str(bet_response.amount)).as_tuple().exponent * (-1) > 2:
        raise HTTPException(status_code=400, detail="Too many decimal places")

    response = await requests.get(f"{Settings().LINE_PROVIDER_URL}/events")
    response_json = await response.json()

    events_model = models.EventList(events=response_json["events"])
    event = None

    for e in events_model.events:
        if e.event_id == bet_response.event_id:
            event = e

    if not event:
        raise HTTPException(status_code=404, detail="Event not found")

    row_bet = bet_response.dict()
    # Mock user_id
    row_bet.update({"user_id": "12345", "coefficient": event.coefficient})

    bet = models.CreateBet(**row_bet)

    await storage.create_bet(bet)

    return Response(status_code=201)


@routers.put("/bets/{event_id}/change_state")
async def update_bet(
    event_id: str = Path(), event_result_response: models.BetResultResponse = Body()
) -> Response:
    await storage.update_bet(event_id, event_result_response.state)
    return Response(status_code=200)


@routers.get(
    "/bets/{user_id}", response_model=models.BetList, response_model_by_alias=False
)
async def get_bets(user_id: str = Path()) -> models.BetList:
    #  Mock user_id 12345

    return await storage.get_bets(user_id)
