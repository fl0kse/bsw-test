import datetime
from typing import Any, Union, cast

import pydantic
from bson import ObjectId


class MongoModel(pydantic.BaseModel):
    class Config(pydantic.BaseConfig):
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {
            datetime.datetime: lambda dt: dt.isoformat(),
            ObjectId: lambda oid: str(oid),
        }


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls) -> Any:
        yield cls.validate

    @classmethod
    def validate(cls, v: Union[ObjectId, str]) -> ObjectId:
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid ObjectId")
        if type(v) == str:
            return ObjectId(v)
        return cast(ObjectId, v)

    @classmethod
    def __modify_schema__(cls, field_schema: Any) -> None:
        field_schema.update(type="string")
