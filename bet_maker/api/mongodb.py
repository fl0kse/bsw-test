import logging
from functools import wraps
from typing import Any, Awaitable, Callable

from motor.motor_asyncio import AsyncIOMotorClient

from .config import Settings

logger = logging.getLogger(__name__)

db_client: AsyncIOMotorClient = None


class StartUpError(Exception):
    """Raised if server failed to configure"""


def mongo_request(f: Callable[[Any], Awaitable[Any]]) -> Any:
    @wraps(f)
    async def decorated(*args: Any, **kwargs: Any) -> Any:
        return await f(db_client, *args, **kwargs)

    return decorated


async def connect_db() -> None:
    """Create database connection."""
    global db_client
    try:
        client = AsyncIOMotorClient(Settings().MONGO_SRV)
        db = client["test-db"]
        await db.command("buildinfo")  # prevent cold start
    except Exception as e:
        logger.error(f"Failed to connect to the database: {e}")
        raise StartUpError("Failed to configure Mongodb")
    db_client = db
