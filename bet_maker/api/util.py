from decimal import Decimal

from bson.decimal128 import Decimal128


def convert_decimal(dict_item):
    # This function iterates a dictionary looking for types of Decimal and converts them to Decimal128
    # Embedded dictionaries and lists are called recursively.
    if dict_item is None:
        return None

    for key, value in list(dict_item.items()):
        if isinstance(value, dict):
            convert_decimal(value)
        elif isinstance(value, list):
            for item in value:
                convert_decimal(item)
        elif isinstance(value, Decimal):
            dict_item[key] = Decimal128(str(value))

    return dict_item
