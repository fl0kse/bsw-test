from pydantic import BaseSettings


class Settings(BaseSettings):
    MONGO_SRV: str
    LINE_PROVIDER_URL: str
