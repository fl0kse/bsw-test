import decimal
from enum import IntEnum

from pydantic import BaseModel, Field

from .pymongo_model import MongoModel, PyObjectId


class EventState(IntEnum):
    NEW = 1
    FINISHED_WIN = 2
    FINISHED_LOSE = 3


class Event(BaseModel):
    event_id: str
    coefficient: decimal.Decimal
    deadline: int
    state: EventState = EventState.NEW


class EventList(BaseModel):
    events: list[Event]


class BetResponse(BaseModel):
    event_id: str
    amount: decimal.Decimal


class CreateBet(BaseModel):
    event_id: str
    user_id: str
    amount: decimal.Decimal
    coefficient: decimal.Decimal
    state: EventState = EventState.NEW.value


class Bet(MongoModel):
    bet_id: PyObjectId = Field(alias="_id")
    event_id: str
    user_id: str
    amount: decimal.Decimal
    coefficient: decimal.Decimal
    state: EventState


class BetList(MongoModel):
    bets: list[Bet]


class BetResultResponse(BaseModel):
    state: EventState

    class Config:
        use_enum_values = True
