from motor.motor_asyncio import AsyncIOMotorClient

from . import models, mongodb
from .util import convert_decimal


@mongodb.mongo_request  # type: ignore
async def create_bet(db: AsyncIOMotorClient, bet: models.CreateBet) -> None:
    await db.bets.insert_one(convert_decimal(bet.dict()))


@mongodb.mongo_request  # type: ignore
async def get_bets(db: AsyncIOMotorClient, user_id: str) -> models.BetList:
    documents = db.bets.find({"user_id": user_id})
    bet_list = []
    async for document in documents:
        bet_list.append(models.Bet(**document))
    return models.BetList(bets=bet_list)


@mongodb.mongo_request
async def update_bet(db: AsyncIOMotorClient, event_id: str, state: models.EventState):
    await db.bets.update_many({"event_id": event_id}, {"$set": {"state": state}})
