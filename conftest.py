import pytest as pytest
from pymongo import MongoClient

db_conn: MongoClient | None = None


@pytest.fixture
def line_provider_url() -> str:
    return "http://0.0.0.0:8081"


@pytest.fixture
def bet_maker_url() -> str:
    return "http://0.0.0.0:8080"


@pytest.fixture
def db() -> MongoClient:
    global db_conn
    if not db_conn:
        db_conn = MongoClient("mongodb://0.0.0.0:27017/")
    yield db_conn["test-db"]
    # should I tell you that this should never come close to prod?
    db_conn.drop_database("test-db")
