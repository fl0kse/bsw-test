lint:
	black .
	isort .
	flake8 .

type:
	mypy --strict .